KephaLess
=========

Fourni un framework less pour générer les feuilles de styles des sites et
applications du projet Théo-Net.

Au tout début, au printemps 2015, je me suis inspiré du framework KNACSS 
https://github.com/alsacreations/KNACSS dans sa version 4.*.* pour débuter
mon travail sur ce framework. Depuis ce temps et jusqu'au premier commit, il
y a eu beaucoup de travail. 


Licence
-------

Regardez le fichier [LICENCE](/LICENCE).

