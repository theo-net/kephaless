'use strict' ; 

/**
 * gulpfile.js
 */

const gulp         = require('gulp'),
      less         = require('gulp-less'),
      autoprefixer = require('gulp-autoprefixer'),
      uglifycss    = require('gulp-uglifycss'),
      rimraf       = require('rimraf') ;


/**
 * Tâches générales
 */
gulp.task('clean', cb => {

  rimraf('dist', cb) ;
}) ;

gulp.task('build', ['css', 'fonts', 'img', 'js'], () => {}) ;


gulp.task('watch', ['build'], () => {
  
  gulp.watch('src/less/**/*.less', ['css']) ;
  gulp.watch('src/fonts/**/*', ['fonts']) ;
  gulp.watch('src/img/*', ['img']) ;
  gulp.watch('src/js/*', ['js']) ;
}) ;


/**
 * Sous-tâches
 */

gulp.task('css', () => {
  
  gulp.src('src/less/styles.less')
    .pipe(less())
    .pipe(autoprefixer('last 2 versions', '> 1 %'))
    .pipe(gulp.dest('dist/css')) ;
  gulp.src('src/less/styles.min.less')
    .pipe(less())
    .pipe(autoprefixer('last 2 versions', '> 1 %'))
    .pipe(uglifycss())
    .pipe(gulp.dest('dist/css')) ;
}) ;

gulp.task('fonts', () => {

  gulp.src('node_modules/kephafont/dist/font/*')
      .pipe(gulp.dest('dist/fonts/KephaFont')) ;
  gulp.src('node_modules/kephafont/dist/list.json')
      .pipe(gulp.dest('dist/')) ;
  gulp.src('src/fonts/**/*')
      .pipe(gulp.dest('dist/fonts')) ;
}) ;

gulp.task('img', () => {

  gulp.src('src/img/*')
      .pipe(gulp.dest('dist/img')) ;
}) ;

gulp.task('js', () => {

  gulp.src('src/js/*')
      .pipe(gulp.dest('dist/js')) ;
}) ;

