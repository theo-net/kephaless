'use strict' ;

/**
 * NetworkBar
 */

// Déploie un volet latéral lorsque l'on clique sur le logo de la barre
// headerNetwork
document.querySelector('#NetworkBar__logo').onclick = e => {

  e.preventDefault() ;
    
  let body = document.querySelector('body') ;
  body.classList.toggle('with--sidebarL') ;
} ;

// Déploie un volet latéral lorsque l'on clique sur le nom d'utilisateur
// headerNetworkUser
let userBtn = document.querySelector('#NetworkBar__user__button') ;
if (userBtn) {
  
  userBtn.onclick = e => {

    e.preventDefault() ;
    
    let body = document.querySelector('body') ;
    body.classList.toggle('with--sidebarUser') ;
  } ;
}

// Déploie un volet latéral lorsque l'on clique sur le bouton de la barre
// headerSite
document.querySelector('#headerSite__button').onclick = e => {

  e.preventDefault() ;
    
  let body = document.querySelector('body') ;
  body.classList.toggle('with--sidebarR') ;
} ;

// Cache les volets latéraux lorsque l'on clique sur le fond grisé
document.querySelector('#site-cache').onclick = e => {

  e.preventDefault() ;
    
  let body = document.querySelector('body') ;
  body.classList.remove('with--sidebarL') ;
  body.classList.remove('with--sidebarR') ; 
  body.classList.remove('with--sidebarUser') ; 
} ;

// Change la classe du site-container lorsque l'on scrool vers le bas.
// Dans notre cas, cela permet de modifier l'affichage
let siteContent = document.querySelector('.site-content') ;
siteContent.onscroll = K.throttle(() => {

  let header = document.querySelector('#site-container') ;
  
  if (siteContent.scrollTop > 50)
    header.classList.add('shrink') ;
  else
    header.classList.remove('shrink') ;
  
}, 100) ;


/**
 * Permet de déployer les menu accordéons
 */

document.body.addEventListener('mousedown', e => {

  let element = null ; 

  let target = e.target || e.srcElement ; 
  while (target.parentElement !== null) {

    // On se prémuni d'un click sur un lien du sous-menu
    if (   target.parentElement.classList.contains('has-sub')
        && target.tagName != 'UL') {
      element = target.parentElement ; 
      break ; 
    }
    target = target.parentElement ; 
  }
 
  // Si aucun élément .wave-effect on abandonne, it's a TRAP ! 
  if (element === null ) 
    return false ; 

  element.classList.toggle('deployed') ; 
}, false) ;


/**
 * Effet vague
 */
  
document.body.addEventListener('mousedown', show, false) ; 

// Cette fonction permet d'afficher une vague suite à un évènement
function show (e) {

  let element = null ; 

  // On regarde si l'évènement affecte un élément avec la class .wave-effect
  let target = e.target || e.srcElement ; 
  while (target.parentElement !== null) {
    if (target.classList.contains('wave-effect')) {
      element = target ; 
      break ; 
    }
    target = target.parentElement ; 
  }

  // Si aucun élément .wave-effect on abandonne, it's a TRAP ! 
  if (element === null ) 
    return false ; 

  // On créé l'élément wave et on l'ajoute à notre élément
  let wave = document.createElement('div') ; 
  wave.className = 'wave' ; 
  element.appendChild(wave) ; 

  // On anime la transformation scale() sans oublier les préfixes...
  let position = getRelativeEventPosition(element, e) ; 
  let radius = getRadius(element, position) ; 
  let scale = 'scale(1)' ; 
  wave.style.left = (position.x - radius) + 'px' ; 
  wave.style.top  = (position.y - radius) + 'px' ; 
  wave.style.width  = (radius * 2) + 'px' ; 
  wave.style.height = (radius * 2) + 'px' ; 
  wave.style['-webkit-transform'] = scale ; 
  wave.style.transform = scale ; 

  // Quand on quitte le bouton
  element.addEventListener('mouseup', hide, false) ; 
  element.addEventListener('mouseleave', hide, false) ; 
  if ('ontouchstart' in window) 
    document.body.addEventListener('touchend', hide, false) ; 
}

// Permet de récupérer la position d'un élément sur la page
function getRelativeEventPosition (element, e) {

  let offset = {
    top:  element.getBoundingClientRect().top  + window.pageYOffset - element.clientTop,
    left: element.getBoundingClientRect().left + window.pageXOffset - element.clientLeft
  } ;

  return {
    y: e.pageY - offset.top,
    x: e.pageX - offset.left
  } ; 
}

// Permet d'obtenir le rayon d'un cercle qui contiendra tout l'élément
function getRadius (element, position) {

  let w = Math.max(position.x, element.clientWidth  - position.x) ; 
  let h = Math.max(position.y, element.clientHeight - position.y) ; 
  
  return Math.ceil(Math.sqrt(Math.pow(w, 2) + Math.pow(h, 2))) ; 
}

// Déclenché au moment d'un release, on masque la dernière vague présente
function hide (e) {

  let element = this ; 

  // On trouve le dernier élément .wave
  let wave  = null ; 
  let waves = element.getElementsByClassName('wave') ; 
  if (waves.length > 0) 
    wave = waves[waves.length - 1] ; 
  else 
    return false ; 
  
  // On fait disparaître la vague en opacité
  wave.style.opacity = 0 ; 

  // On supprime l'élément vague au bout de la durée de l'animation
  setTimeout(() => {
    try {
      element.removeChild(wave) ; 
    } catch (e) {
      return false ; 
    }
  }, 2000) ; 
}


/**
 * Alert-dismissable
 */

document.body.addEventListener('mousedown', e => {

  let element = null ; 

  // On regarde si l'évènement affecte un élément avec la class .wave-effect
  let target = e.target || e.srcElement ; 
  while (target.parentElement !== null) {
    if (   target.classList.contains('close') 
        && target.parentElement.classList.contains('alert-dismissable')) {
      element = target.parentElement ; 
      break ; 
    }
    target = target.parentElement ; 
  }

  // Si aucun élément, it's a TRAP ! 
  if (element === null ) 
    return false ; 
 
  // On efface l'alerte
  element.remove() ;  
}, false) ; 

